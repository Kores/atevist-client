val base_version: String by project
val ktor_version: String by project
val kores_version: String by project
val kotlin_version: String by project
val kotlinx_immutable_version: String by project
val logback_version: String by project
val prometeus_version: String by project
val eventsys_version: String by project
val eventsys_coroutine_version: String by project
val config_version: String by project
val jwiutils_version: String by project
val tomlj_version: String by project
val jackson_version: String by project
val redin_version: String by project
val fastutil_version: String by project

plugins {
    application
    kotlin("jvm") version "1.9.0"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.9.0"
    id("com.github.hierynomus.license") version "0.16.1"
    id("maven-publish")
}

group = "com.github.koresframework"
version = rootProject.sourceSets.main.get().resources.find { it.name == "com.koresframework.atevist.client.version" }!!.readText()

application {
    mainClass.set("com.github.koresframework.ApplicationKt")
}

repositories {
    mavenCentral()
    maven(url = "https://gitlab.com/api/v4/projects/30392813/packages/maven") {
        name = "JGang"
    }
}

dependencies {
    api("com.koresframework:atevist-base:$base_version")

    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version")
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    api("org.jetbrains.kotlinx:kotlinx-collections-immutable:$kotlinx_immutable_version")

    api("com.github.koresframework:eventsys:$eventsys_version")
    api("com.koresframework:eventsys-coroutine:$eventsys_coroutine_version")
    api("com.github.jonathanxd:jwiutils:$jwiutils_version")
    api("com.github.jonathanxd:jwiutils-kt:$jwiutils_version")
    api("com.github.jonathanxd:specializations:$jwiutils_version")
    api("com.github.jonathanxd:links:$jwiutils_version")
    api("com.github.jonathanxd:properties:$jwiutils_version")
    api("com.github.jonathanxd:json-lang-loader:$jwiutils_version")
    implementation("com.github.jonathanxd:redin:$redin_version")
    implementation("it.unimi.dsi:fastutil:$fastutil_version")

    implementation("com.fasterxml.jackson.core:jackson-databind:$jackson_version")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jackson_version")

    api("com.koresframework:kores:$kores_version.base")
    implementation("com.koresframework:kores-bytecodewriter:$kores_version.bytecode")
    implementation("io.ktor:ktor-client-serialization:$ktor_version")
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-cio:$ktor_version")
    implementation("io.ktor:ktor-client-websockets:$ktor_version")
    implementation("io.ktor:ktor-client-auth:$ktor_version")

    implementation("ch.qos.logback:logback-classic:$logback_version")
    testImplementation(kotlin("test"))
    testImplementation("org.jetbrains.kotlin:kotlin-scripting-jvm:$kotlin_version")
    testImplementation("org.jetbrains.kotlin:kotlin-scripting-jvm-host:$kotlin_version")

    testImplementation("io.kotest:kotest-runner-junit5:4.6.1")
    testImplementation("io.kotest:kotest-assertions-core:4.6.1")
    testImplementation("io.kotest:kotest-property:4.6.1")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.2")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>() {
    kotlinOptions.jvmTarget = "19"
    kotlinOptions.freeCompilerArgs = listOf("-Xjvm-default=all")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks {
    withType<nl.javadude.gradle.plugins.license.License> {
        header = rootProject.file("LICENSE")
        strictCheck = true
    }
}


val sourcesJar = tasks.create<Jar>("sourcesJar") {
    dependsOn("classes")
    this.archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    repositories {
        maven {
            // change to point to your repo, e.g. http://my.org/repo
            url = uri("${layout.buildDirectory}/repo")
        }
        maven {
            name = "GitLab"
            url = uri("https://gitlab.com/api/v4/projects/29601219/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                val ciToken = System.getenv("CI_JOB_TOKEN")
                if (ciToken != null && ciToken.isNotEmpty()) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                } else {
                    name = "Private-Token"
                    value = project.findProperty("GITLAB_TOKEN")?.toString() ?: System.getenv("GITLAB_TOKEN")
                }
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
        maven {
            name = "GitLabJgang"
            url = uri("https://gitlab.com/api/v4/projects/30392813/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                val ciToken = System.getenv("CI_JOB_TOKEN")
                if (ciToken != null && ciToken.isNotEmpty()) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                } else {
                    name = "Private-Token"
                    value = project.findProperty("GITLAB_TOKEN")?.toString() ?: System.getenv("GITLAB_TOKEN")
                }
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
    }
    publications {
        register("maven", MavenPublication::class) {
            from(components["kotlin"])
            artifactId = "atevist-client"
            artifact(sourcesJar)
        }
    }
}