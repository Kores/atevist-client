/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.client

import com.koresframework.atevist.base.*
import com.koresframework.atevist.base.data.AtevistEvent
import com.koresframework.atevist.base.data.MultiWatchRequest
import com.koresframework.atevist.client.data.Server
import com.koresframework.atevist.client.data.ServerVersion
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.auth.*
import io.ktor.client.features.auth.providers.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.websocket.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import java.net.ConnectException
import java.time.Duration
import java.time.ZonedDateTime
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.ConcurrentSkipListSet
import java.util.concurrent.atomic.AtomicReference
import kotlin.math.max

val VERSION = AtevistClient::class.java.classLoader
    .getResourceAsStream("com.koresframework.atevist.client.version")!!
    .bufferedReader()
    .readLine()

val API_VERSION = AtevistClient::class.java.classLoader
    .getResourceAsStream("com.koresframework.atevist.base.version")!!
    .bufferedReader()
    .readLine()

val JSON = Json {
    encodeDefaults = true
    isLenient = true
    allowSpecialFloatingPointValues = true
    allowStructuredMapKeys = true
    prettyPrint = false
    //useArrayPolymorphism = true
    classDiscriminator = "_type"
    ignoreUnknownKeys = true
}

fun Atevist(
    addresses: List<String>,
    login: String,
    pwd: String,
    reconnect: Duration = Duration.ofSeconds(30),
    retries: Retries = Retries.Times(10, interval = Duration.ofSeconds(5)),
    dispatcher: CoroutineDispatcher = EVENT_DISPATCHER
) =
    AtevistClient(HttpClient(CIO) {
        install(WebSockets)
        install(JsonFeature) {
            serializer = KotlinxSerializer(JSON)
        }
        install(Auth) {
            basic {
                credentials {
                    BasicAuthCredentials(username = login, password = pwd)
                }
            }
        }
    }, addresses, reconnect, retries, dispatcher)

/**
 * Atevist Client instance, it starts by creating a dedicated connection subscribed to `@all` and `all` channels,
 * and a worker Coroutine to dispatch events to the server.
 *
 * You can subscribe to additional channels by invoking [subscribe] function, which starts a new dedicated connection
 * to subscribe all provided channels at once.
 *
 * It also handles retrial and reconnection based on [retries] and [reconnect] properties.
 *
 * @property retries Configuration to retry to send events when server fails or the connection is lost. In case
 * of lost connection, it respects the [reconnect]. In other words, if the event fails to be sent because
 * the connection was lost, it will retry to send the event after the reconnection is made, which respects the
 * [reconnection interval][reconnect].
 * @property reconnect The interval to wait before retrying to reconnect to the server.
 * @property dispatcher The [CoroutineDispatcher] to use to launch jobs for subscription and event dispatch.
 * @property retention The default retention for events with redundancy set without lifetime specified.
 */
class AtevistClient(
    private val client: HttpClient,
    val addresses: List<String>,
    val reconnect: Duration = Duration.ofSeconds(30),
    override val retries: Retries,
    val dispatcher: CoroutineDispatcher = EVENT_DISPATCHER,
    override val retention: Duration = Duration.ofMinutes(10)
): Atevist {
    val apiVersion
        get() = API_VERSION

    val version
        get() = VERSION

    override val listenFlow: EventListenFlow = EventListenFlowImpl(this.retries, CoroutineScope(EVENT_DISPATCHER))
    private val dispatch: Channel<AtevistEvent> = Channel()
    private val subscribedChannels = ConcurrentSkipListSet<String>()
    override val logger = LoggerFactory.getLogger(AtevistClient::class.java)
    override val dispatchChannel: ReceiveChannel<AtevistEvent> get() = this.dispatch
    override val id = UUID.randomUUID()

    private suspend fun check() {
        for (version in serverVersions()) {
            check(version)
        }
    }

    private suspend fun check(address: String) {
        val server = serverVersion(address)
        check(server)
    }

    private fun check(server: Server) {
        val serverApiVersion = server.version.apiVersion
        if (serverApiVersion != apiVersion) {
            logger.warn("Server `${server.address}` has API Version `${serverApiVersion}` but client has `$apiVersion`. They may be incompatible.")
        }
    }

    suspend fun serverVersions(): List<Server> {
        return addresses.map { address ->
            serverVersion(address)
        }
    }

    suspend fun serverVersion(address: String): Server {
        val host = address.split(":")[0]
        val port = address.split(":")[1].toInt()

        val data = client.get<ServerVersion>(host = host, port = port, path = "/status")

        return Server(
            address = address,
            version = data
        )
    }

    private suspend fun connectMulti(channels: Set<String>): List<Job> {
        return addresses.map { address ->
            val host = address.split(":")[0]
            val port = address.split(":")[1].toInt()

            CoroutineScope(dispatcher).launch {
                val lastMsg = AtomicReference<ZonedDateTime>(ZonedDateTime.now())
                while (true) {
                    try {
                        client.webSocket(HttpMethod.Get, host = host, port = port, path = "/poll/multi") {
                            send(JSON.encodeToString(MultiWatchRequest(id, channels, lastMsg.get())))
                            logger.info("Connected to Atevist at `$address` for channels: `${channels}`.")
                            onConnect(address)

                            while(true) {
                                withTimeoutOrNull(5000) {
                                    val receive = incoming.receiveCatching()
                                    if (receive.isFailure) {
                                        throw receive.exceptionOrNull()!!
                                    } else if (receive.isSuccess) {
                                        val ev = receive.getOrThrow() as Frame.Text
                                        val event = JSON.decodeFromString<AtevistEvent>(ev.readText())
                                        if (lastMsg.get().isBefore(event.createdAt))
                                            lastMsg.set(event.createdAt)
                                        dispatch.send(event)
                                    } else {
                                        throw receive.exceptionOrNull()!!
                                    }
                                }
                            }
                        }
                        break
                    } catch (e: ConnectException) {
                        logger.error("Failed to connect to Atevist, retrying in ${reconnect}...")
                        delay(reconnect.toMillis())
                    } catch (t: Throwable) {
                        logger.error("Exception occurred, retrying in ${reconnect}...", t)
                        delay(reconnect.toMillis())
                    }
                }
            }
        }
    }

    /**
     * Creates a new connection subscribing to a single [channel] (if it is not already subscribed)
     */
    fun subscribe(channel: String) {
        subscribe(setOf(channel))
    }

    /**
     * Creates a new connection subscribing to provided [channels].
     *
     * Every call to [subscribe] creates a new connection that handles all provided [channels],
     * it is recommended to provide all channels to subscribe at the start of the application.
     *
     * It only subscribes to channel that is not already subscribed.
     */
    fun subscribe(channels: Set<String>) {
        val channelsToSub = channels.filterTo(mutableSetOf()) { this.subscribedChannels.add(it) }
        CoroutineScope(dispatcher).launch {
            while (true) {
                try {
                    connectMulti(channelsToSub).forEach { it.join() }
                } catch (t: Throwable) {
                    t.printStackTrace()
                    logger.error("Exception occurred while subscribed to channels `$channelsToSub`", t)
                }
                logger.error("Disconnected from Atevist, reconnecting in ${reconnect}...")
                delay(reconnect.toMillis())
            }
        }
    }

    init {
        CoroutineScope(dispatcher).launch {
            subscribe(setOf("@all", "all"))
        }
        CoroutineScope(dispatcher).launch {
            while (true) {
                try {
                    send().forEach { it.join() }
                } catch (t: Throwable) {
                    t.printStackTrace()
                    logger.error("Exception occurred while sending messages", t)
                }
                logger.error("Disconnected from Atevist, reconnecting in ${reconnect}...")
                delay(reconnect.toMillis())
            }
        }
    }

    private suspend fun send(): List<Job> {
        return addresses.map { address ->
            val host = address.split(":")[0]
            val port = address.split(":")[1].toInt()
            data class Send(val event: AtevistEvent, val complete: CompletableDeferred<Unit>)
            val sendChannel = Channel<Send>()
            CoroutineScope(dispatcher).launch {
                while (true) {
                    try {
                        client.webSocket(HttpMethod.Get, host = host, port = port, path = "/dispatch") {
                            logger.info("Connected to Atevist at `$address` for event dispatch.")
                            onConnect(address)
                            try {
                                while (true) {
                                    val received = sendChannel.receive()
                                    try {
                                        sendEvent(received.event)
                                        incoming.receive()
                                        received.complete.complete(Unit)
                                    } catch (t: Throwable) {
                                        received.complete.completeExceptionally(t)
                                        logger.error("Failed to send event to Atevist.", t)
                                        break
                                    }
                                }
                            } catch (t: Throwable) {
                                logger.error("Failed to receive event from channel.", t)
                            }
                        }
                    } catch (e: ConnectException) {
                        logger.error("Failed to connect to Atevist, retrying in ${reconnect}...")
                    } catch (t: Throwable) {
                        logger.error("Exception occurred, retrying in ${reconnect}...", t)
                    } finally {
                        logger.info("Connection finished, reconnecting in ${reconnect}...")
                        delay(reconnect.toMillis())
                    }
                }
            }

            CoroutineScope(dispatcher).launch {
                listenFlow.collectRetrying(id) {
                    val send = Send(it, CompletableDeferred())
                    sendChannel.send(send)
                    try {
                        Result.success(send.complete.await())
                    } catch (e: Throwable) {
                        Result.failure(e)
                    }
                }
            }
        }
    }

    private fun onConnect(address: String) {
        CoroutineScope(dispatcher).launch {
            check(address)
        }
    }

}

suspend fun WebSocketSession.sendEvent(event: AtevistEvent): Unit =
    send(JSON.encodeToString(event))