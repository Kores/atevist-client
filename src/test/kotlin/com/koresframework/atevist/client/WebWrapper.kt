/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.client

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.koresframework.atevist.base.createManager
import com.koresframework.atevist.base.event.AtevistBaseEvent
import com.koresframework.atevist.base.event.RemoteEvent
import com.koresframework.atevist.base.event.ReplyingEvent
import com.koresframework.atevist.base.event.characteristic.Redundancy
import com.koresframework.atevist.base.event.characteristic.RedundancyValue
import com.koresframework.atevist.base.event.characteristic.Reply
import com.koresframework.atevist.base.event.characteristic.Replying
import com.koresframework.atevist.base.events.ATEVIST_CHANNEL_ID
import com.koresframework.atevist.base.events.PingEvent
import com.koresframework.atevist.base.events.PongEvent
import com.koresframework.atevist.base.on
import com.koresframework.eventsys.event.annotation.Extension
import com.koresframework.eventsys.event.annotation.Listener
import com.koresframework.eventsys.event.characteristic.CharacteristicHolder
import com.koresframework.eventsys.event.characteristic.CharacteristicKey
import com.koresframework.eventsys.event.characteristic.CharacteristicValue
import com.koresframework.eventsys.util.createFactory
import io.kotest.core.spec.style.FunSpec
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.onStart
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

object Main

class WebWrapper : FunSpec({
    xtest("test") {
        val client = Atevist(listOf("localhost:5431", "localhost:5432"), "atevist", "123456")

        for (serverVersion in client.serverVersions()) {
            println("Version = $serverVersion")
        }

        val manager = client.createManager()
        val generator = manager.eventGenerator
        manager.eventListenerRegistry.registerListeners(this, MyListen())

        val factory = generator.createFactory<Factory>().resolve()
        val message = factory.createMessageEvent("Hello world")
        val bool = factory.createBoolEvent(true)
        delay(5000)
        val messageAll = factory.createMessageEvent("Hello world")
            .characteristic(MyCharKey, MyCharValue("Jelly"))
            .characteristic(Redundancy, RedundancyValue(true))
        manager.dispatch(event = messageAll, dispatcher = Main, channel = "all")

        while(true) {
            delay(1000)
        }
        /*
        val messageB = factory.createMessageEvent("Hello world")
        val remoteMessageB = factory.createRemoteMessageEvent("Hello world", client.id, UUID.randomUUID())
        for (i in 0..10) {
            manager.dispatch(event = messageAll, dispatcher = Main, channel = "all")
        }
        manager.dispatch(event = messageB, dispatcher = Main, channel = "b")
        manager.dispatch(event = messageB, dispatcher = Main, channel = "b")
        manager.dispatch(event = remoteMessageB, dispatcher = Main, channel = "all")
        manager.dispatch(event = bool, dispatcher = Main, channel = "all")
        //delay(10000)
        val ping = factory.createPing()
        val replied = manager.on<PongEvent>()
            .consumeAsFlow()
            .onStart {
                manager.dispatch(event = ping, dispatcher = Main, channel = ATEVIST_CHANNEL_ID)
            }
            .first { it.isInReplyTo(ping) }


        println("Replied with -------------> $replied")*/

        /*val total = 100_000
    val jobs = ArrayList<Job>(total)
    for (x in 0..total) {
        jobs += CoroutineScope(Dispatchers.IO).launch {
            for (x in 0..10) {
                delay(1L)
                manager.dispatch(message, Main)
            }
        }
    }

    for (job in jobs) {
        job.join()
    }
    println("Sent")*/
    }
})

@JsonIgnoreProperties(ignoreUnknown = true)
object MyCharKey : CharacteristicKey<String> {
    override val id: String
        get() = "foo.bar.MyKey"
}

data class MyCharValue(val value: String) : CharacteristicValue<MyCharKey, String>

interface MessageEvent : AtevistBaseEvent, CharacteristicHolder<MessageEvent> {
    val message: String
}

interface BoolEvent : AtevistBaseEvent {
    val status: Boolean
}

interface Factory {
    fun createMessageEvent(message: String): MessageEvent

    fun createBoolEvent(status: Boolean): BoolEvent

    @Extension(implement = RemoteEvent::class)
    fun createRemoteMessageEvent(message: String, senderClient: UUID, eventUniqueId: UUID): MessageEvent

    fun createPing(): PingEvent
}

class MyListen {
    val receiveCounter = AtomicInteger()

    @Listener
    fun onMessageEvent(event: MessageEvent) {
        if (event is RemoteEvent) {
            println("[${receiveCounter.incrementAndGet()}/x]Received message: ${event.message}. Info: $event")
        }
    }

    @Listener
    fun onBoolEvent(event: BoolEvent) {
        if (event is RemoteEvent) {
            println("[${receiveCounter.incrementAndGet()}/x]Received status: ${event.status}. Info: $event")
        }
    }

    @Listener
    fun onPongEvent(event: PongEvent) {
        if (event is RemoteEvent) {
            println("PONGI: $event")
        }
    }
}

fun AtevistBaseEvent.isInReplyTo(other: AtevistBaseEvent) =
    (this as? CharacteristicHolder<*>)?.characteristics?.get(Reply)?.let {
        ((it as? Replying)?.event as? AtevistBaseEvent)?.eventUniqueId == other.eventUniqueId
    } ?: (this as? ReplyingEvent)?.inReplyTo?.let {
        (it as? AtevistBaseEvent)?.eventUniqueId == other.eventUniqueId
    } ?: false