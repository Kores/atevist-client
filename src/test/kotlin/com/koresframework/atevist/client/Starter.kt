/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.client

import com.koresframework.atevist.base.AtevistManager
import com.koresframework.atevist.base.createManager
import com.koresframework.atevist.base.event.AtevistBaseEvent
import com.koresframework.eventsys.event.EventListener
import com.koresframework.eventsys.result.ListenResult
import com.koresframework.eventsys.util.createFactory
import kotlinx.coroutines.coroutineScope
import java.lang.reflect.Type
import javax.script.ScriptEngineManager

object Starter

/**
 * Examples:
 *
 * `Client 1`
 * ```
 * subscribe(channel = "foo", type = typeOf<AtevistBaseEvent>()) { println(it) }
 * ```
 *
 * `Client 2`
 * ```
 * dispatch(channel = "foo", event = createPing())
 * ```
 */
suspend fun main(): Unit = coroutineScope {
    val client = Atevist(listOf("localhost:5431"/*, "localhost:5432"*/), "atevist", "123456")
    val subs = mutableSetOf<String>()
    val subscriber = object : Subscriber {
        override fun subscribe(channel: String) {
            if (!subs.contains(channel)) {
                subs.add(channel)
                client.subscribe(channel)
            }
        }
    }

    for (serverVersion in client.serverVersions()) {
        println("Version = $serverVersion")
    }

    val manager = client.createManager()
    val generator = manager.eventGenerator
    manager.eventListenerRegistry.registerListeners(this, MyListen())

    val factory = generator.createFactory<Factory>().resolve()
    val engine = ScriptEngineManager().getEngineByExtension("kts")

    engine.put("factory", factory)

    do {
        val line = readlnOrNull()
        if (line != null) {
            try {
                val r = engine.eval(
                    """
                        import com.koresframework.atevist.client.dispatch;
                        import com.koresframework.atevist.client.subscribe;
                        import kotlin.reflect.full.functions;
                        import com.koresframework.atevist.client.Factory;
                        import com.koresframework.atevist.base.event.AtevistBaseEvent;
                        import com.koresframework.kores.type.typeOf;
                        val factory = bindings["factory"] as Factory;                        
                        with(factory) {
                            $line
                        }
                    """.trimIndent()
                ) as Command
                r.run(manager, subscriber)
            } catch (t: Throwable) {
                t.printStackTrace()
            }
        }
    } while (line != null)

}

interface Subscriber {
    fun subscribe(channel: String)
}

interface Command {
    suspend fun run(manager: AtevistManager, subscriber: Subscriber)
}

@Suppress("unused")
fun dispatch(channel: String,
             event: AtevistBaseEvent) = Dispatch(channel, event)

class Dispatch(
    val channel: String,
    val event: AtevistBaseEvent
): Command {
    override suspend fun run(manager: AtevistManager, subscriber: Subscriber) {
        manager.dispatch(
            event = event,
            dispatcher = Starter,
            channel = channel
        )
        println("Sent `${event::class.simpleName}` to channel `${channel}`")
    }
}

@Suppress("unused")
fun subscribe(
    channel: String,
    type: Type,
    onEvent: suspend (AtevistBaseEvent) -> Unit
): Subscribe = Subscribe(channel, type, onEvent)

class Subscribe(
    val channel: String,
    val type: Type,
    val onEvent: suspend (AtevistBaseEvent) -> Unit
): Command {
    override suspend fun run(manager: AtevistManager, subscriber: Subscriber) {
        subscriber.subscribe(channel)
        manager.eventListenerRegistry.registerListener(
            this,
            this.type,
            object : EventListener<AtevistBaseEvent> {
                override val channel: String
                    get() = this@Subscribe.channel

                override suspend fun onEvent(event: AtevistBaseEvent, dispatcher: Any): ListenResult {
                    println("Received `${event::class.simpleName}` on channel `${channel}`")
                    onEvent(event)
                    return ListenResult.Value(Unit)
                }
            }
        )
        println("Subscribed for `${type}` in channel `${channel}`")
    }
}