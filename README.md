# atevist-client

This is the Kotlin/JVM Atevist client library, it handles event serialization, deserialization, 
dispatching and listening using **EventSys Runtime** bridge with **Atevist** communication.

Atevist server is not released yet and are under development, being integrated with [idocs.xyz](https://idocs.xyz)
as a first test scenario and planned to be released soon.

## Simple

Atevist is a simple event dispatcher, it is built on top of Ktor and WebSockets, which provides a very low latency
communication between clients and servers.

Ktor provides the base for async server and clients programming, Atevist takes advantage of this power to build a fast, reliable
and consistent event dispatch mechanism.

## Built for JVM, on top of EventSys

Atevist client is built for the JVM, on top of EventSys, a dynamic event generation library. 

## Dependency

To add **Atevist-Client** as a dependency of your project, add the repo:

```kotlin
repositories {
    maven(url = "https://gitlab.com/api/v4/projects/30392813/packages/maven") {
        name = "JGang"
    }
}
```

And the dependency:
```kotlin
dependencies {
    implementation("com.github.koresframework:atevist-client:0.0.12-alpha")
}
```

### Connect

Create a client:

```kotlin
fun main() {
    val client = Atevist(listOf(/* clients host:port */ "localhost:5431"), /* username */ "atevist", /* password */ "123456")
}
```

Select the channels to subscribe to:

```kotlin
fun main() {
    val client = Atevist(listOf(/* clients host:port */ "localhost:5431"), /* username */ "atevist", /* password */ "123456")
    client.subscribe(setOf("foo", "bar"))
}
```

Every call to `subscribe` starts a new connection to Atevist, so beware of this, always prefer to provide all channels at once.

### Create a manager and register listener

```kotlin
fun main() {
    val client = Atevist(listOf(/* clients host:port */ "localhost:5431"), /* username */ "atevist", /* password */ "123456")
    client.subscribe(setOf("@all", "all"))
    
    val manager = client.createManager()
    manager.eventListenerRegistry.registerListeners(this, MyListen())
}

interface MessageEvent : AtevistBaseEvent {
    val message: String
}

class MyListen {
    @Listener
    fun onMessageEvent(event: MessageEvent) {
    }
}
```

### Full example

```kotlin
object App
fun main() {
    val client = Atevist(listOf(/* clients host:port */ "localhost:5431"), /* username */ "atevist", /* password */ "123456")
    client.subscribe(setOf("foo", "bar"))

    val manager = client.createManager()
    manager.eventListenerRegistry.registerListeners(this, MyListen())
    val factory = manager.eventGenerator.createFactory<Factory>().resolve()
    
    // Now all you need is the factory and the manager.
    val event = factory.createMessageEvent("Hello world!")
    manager.dispatch(event, App, channel = "foo") // Dispatch to other clients
}

interface MessageEvent : AtevistBaseEvent {
    val message: String
}

interface Factory {
    fun createMessageEvent(message: String): MessageEvent
}

class MyListen {
    @Listener
    fun onMessageEvent(event: MessageEvent) {
        if (event is RemoteEvent) {
            println("Receive the message: ${event.message}")
        }
    }
}
```

You can abstract away the **factory** and **manager** creation using the preferred DI (Dependency Injection) library.